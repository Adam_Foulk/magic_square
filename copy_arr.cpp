#include<iostream>
#include<typeinfo>
#include<vector>
#include<cmath>

int** squareCopy(int** square, int square_side_size) {

	int square_copy[square_side_size][square_side_size];

	for (int row = 0; row < square_side_size; row++) {
		
		for (int column = 0; column < count; column++) {
			
			square_copy[row][column] = square[row][column];
		}
	}

	return square_copy;
}

int** squareInvertRow(int** square, int square_side_size, int row_1, int row_2) {

	int** square_copy = squareCopy(square, square_side_size);

	for (int column = 0; column < square_side_size; column++) {

		square_copy[row_1][column] = square_copy[row_2][column];
	}

	return square_copy;
}

int** squareInvertColumn(int** square, int square_side_size, int column_1, int column_2) {

	int** square_copy = squareCopy(square, square_side_size);

	for (int row = 0; row < square_side_size; row++) {
		
		square_copy[column_1][row] = square_copy[column_2][row];
	}

	return square_copy;
}

int** squareRotate(int** square, square_side_size) {

	int** array = createArray(square, square_side_size);

	for(int column1 = 0, row2 = 0; 
		column1 < square_side_size, row2 < square_side_size; 
		column1++, row2++) {

		for(int row1 = square_side_size-1, column2 = 0; 
			row1 >= 0 || column2 < square_side_size; 
			row1--, column2++) {

			array[row_2][column2] = square[row1][column1];
		}
	}	


	return square_copy;
}

int*** getInvertedSquares(int** square, int square_side_size) {

	int ***squares = new int** [square_side_size];

	int squares_count = 0;

	for(int row_min = 0, row_max = square_side_size-1; 
		row_min < row_max; 
		squares_count++, row_min++, row_max-- ) {

		squares[squares_count] = squareInvertRow(square, square_side_size, row_min, row_max);
	}

	for(int column_min = 0, column_max = square_side_size-1; 
		column_min < column_max; 
		squares_count++, column_min++, column_max-- ) {

		squares[squares_count] = squareInvertColumn(square, square_side_size, column_min, column_max);
	}

	return squares;
}


int*** sumSquares(int*** rotated_square, int rotated_square_number, int**** inverted_squares, int square_side_size) {

	int inverted_squares_number = square_side_size * rotated_square_number;
	
	int all_squares_number = rotated_square_number + inverted_squares_number;
	
	int ***all_squares = new int** [all_squares_number];


	int all_squares_count = 0;

	for(int rotated_square_count = 0; 
		rotated_square_count < rotated_square_number; 
		rotated_square_count++, all_squares_count++) {

		all_squares[all_squares_count] = rotated_square[rotated_square_count];
	}

	for(int inverted_squares_group_count = 0;
		inverted_squares_group_count < rotated_square_number; 
		inverted_squares_group_count++) {

		for(int inverted_squares_count = 0; 
			inverted_squares_count < square_side_size; 
			inverted_squares_count++, all_squares_count++) {

			all_squares[all_squares_count] = inverted_squares[inverted_squares_group_count][inverted_squares_count];
		}
	}

	
	for(int inverted_squares_group_count = 0;
		inverted_squares_group_count < rotated_square_number; 
		inverted_squares_group_count++) {
		
		delete[] inverted_squares[inverted_squares_group_count];
	}
	
	delete[] inverted_squares;
	delete[] rotated_square;
	
	return all_squares;
}
